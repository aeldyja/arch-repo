#!/bin/bash
#shellcheck disable=SC2155

#
# Archlinux Repo
# Filename : repo-update.sh
# Author: Navarra Alice
# Creation Date: 2020-04-25
# Description: Update repository databse, rotate packages
#


#######################################################################
#                               IMPORTS                               #
#######################################################################

#######################################################################
#                              VARIABLES                              #
#######################################################################

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
declare -r IMPORT_DIR="${SCRIPT_DIR}/in"
declare -r REPO_NAME="icp"
declare -r REPO_DIR="${1:-${SCRIPT_DIR}/www}"
declare -r REPO_FILE="${REPO_NAME}.db.tar.gz"
declare -r ARCHIVES_DIR="${SCRIPT_DIR}/archives"
declare -r PKG_EXT=".pkg.tar.zst"

declare -r CMD_REPO_ADD="repo-add -s "

declare -A PACKAGES_INFOS=( [list]="" )
declare -a REPO_ARCH=()

#######################################################################
#                              FUNCTIONS                              #
#######################################################################

##
#===  FUNCTION  ================================================================
#         NAME:  find_new_packages
#  DESCRIPTION:  Search for new packages in IMPORT_DIR
#===============================================================================
function find_new_packages() { #Parameters -- {{{
# Find package(s) in IMPORT_DIR and return them
find "${IMPORT_DIR}" -name "*${PKG_EXT}"
} # ---------- end of function find_new_packages ----------#}}}

#===  FUNCTION  ================================================================
#         NAME:  package_basename
#  DESCRIPTION:  Extract the package name from the package file
#===============================================================================
function package_info() { #Parameters -- 1:(str) package_file {{{
local package_file="${1}"

local -A pkginfo=()
while read -r line; do
    case "$line" in \#*) continue ;; esac
pkginfo["${line% =*}"]="${line#*= }"
done < <(bsdtar -xOqf "${package_file}" ".PKGINFO")

PACKAGES_INFOS['list']+=" ${pkginfo['pkgname']}"
PACKAGES_INFOS["${package_file}"]="${pkginfo['pkgname']}"
for key in "${!pkginfo[@]}"; do
    PACKAGES_INFOS["${pkginfo['pkgname']},${key}"]="${pkginfo["${key}"]}"
done
} # ---------- end of function package_basename ----------#}}}

#===  FUNCTION  ================================================================
#         NAME:  find_old_versions
#  DESCRIPTION:  Find older versions of the package that are currently in the repository
#===============================================================================
function find_old_versions() { #Parameters -- 1:(str) package_name {{{
local package_name="${1}"

# Find file matching the package_name in all the architectures
for arch in ${PACKAGES_INFOS["${package_name},arch"]}; do
    case "${arch}" in
        any)
            find "${REPO_DIR}" -type f -name "${package_name}*${PKG_EXT}"
            ;;
        *)
            if [[ "${REPO_ARCH[*]}" == *" ${arch} "* ]]; then
                find "${REPO_DIR}/${arch}" -type f -name "${package_name}*${PKG_EXT}"
            fi
            ;;
    esac
done
} # ---------- end of function find_old_versions ----------#}}}

#===  FUNCTION  ================================================================
#         NAME:  add_to_repo
#  DESCRIPTION:  Add the package to the repository
#===============================================================================
function add_to_repo() { #Parameters -- 1:(str) package_file {{{
local package_file="$(basename "${1}")"

local package_name="${PACKAGES_INFOS["${1}"]}"
# Move file to repository(s) and add them to the database(s)
for arch in ${PACKAGES_INFOS["${package_name},arch"]}; do
    echo "Copying package \"${package_name}\" to repo"
    case "${arch}" in
        any)
            for rarch in "${REPO_ARCH[@]}"; do
                cp -v "${IMPORT_DIR}/${package_file}" "${REPO_DIR}/${rarch}/${package_file}"
                ${CMD_REPO_ADD} "${REPO_DIR}/${rarch}/${REPO_FILE}" "${REPO_DIR}/${rarch}/${package_file}"
            done
            ;;
        *)
            if [[ "${REPO_ARCH[*]}" == *" ${arch} "* ]]; then
                cp -v "${IMPORT_DIR}/${package_file}" "${REPO_DIR}/${arch}/${package_file}"
                ${CMD_REPO_ADD} "${REPO_DIR}/${arch}/${REPO_FILE}" "${REPO_DIR}/${arch}/${package_file}"
            fi
            ;;
    esac
done

# Remove file from IMPORT_DIR
rm -v "${IMPORT_DIR}/${package_file}"
} # ---------- end of function add_to_repo ----------#}}}

#===  FUNCTION  ================================================================
#         NAME:  archive_package
#  DESCRIPTION:  Remove the package from the repository
#===============================================================================
function archive_package() { #Parameters -- 1:(str) package_file {{{
local package_file="${1}"

# Move file to archives (if not already there)
mapfile -t archs < <(bsdtar -xOqf "${package_file}" ".PKGINFO" | grep "arch" | awk -F '=' '{printf $NF}')
for arch in "${archs[@]}"; do
    mkdir -pv "${ARCHIVES_DIR}/${arch}"
    mv -v "${package_file}" "${ARCHIVES_DIR}/${arch}"
done
} # ---------- end of function archive_package ----------#}}}

#===============================================================================

#===  FUNCTION  ================================================================
#         NAME:  detect_repo_archs
#  DESCRIPTION:  Find all architectures in the repository
#===============================================================================
function detect_repo_archs() { #Parameters -- {{{
mapfile -t REPO_ARCH < <(find "${REPO_DIR}" -mindepth 1 -maxdepth 1 -type d | awk -F '/' '{print $NF}')
} # ---------- end of function detect_repo_archs ----------#}}}

#===  FUNCTION  ================================================================
#         NAME:  usage
#  DESCRIPTION:  Display usage information.
#===============================================================================
function usage () #{{{
{
    echo "Usage :  $0 [options] [--]

    Options:
    -h|help         Display this message
    -R|--remove     Remove a package definitly from the repository [not implemented yet]"

}    # ----------  end of function usage  ----------#}}}

#-----------------------------------------------------------------------
#  Handle command line arguments
#-----------------------------------------------------------------------

# Execute getopt {{{
ARGS=$(getopt -o h,R: -l "help,remove:" -- "$@") || exit 1

eval set -- "${ARGS}";

while true; do
    case ${1} in
        -h|--help)
            usage
            shift;;
        -R|--remove)
            [[ -n "${2}" ]] && {
            #TODO
            shift 2
        } || exit 2;;
    --)
        shift
        break;;
esac
done
#}}}

#######################################################################
#                                MAIN                                 #
#######################################################################

detect_repo_archs

# Check if import dir exist
if [ ! -d "${IMPORT_DIR}" ]; then
    echo "No import directory found at \"${IMPORT_DIR}\""
    exit 1
fi
mapfile -t packages < <(find_new_packages)

for package in "${packages[@]}"; do
    # Get package info
    package_info "${package}"
    mapfile -t packages_to_archives < <(find_old_versions "${PACKAGES_INFOS["${package}"]}")
    add_to_repo "${package}"
done

(( ${#package_to_archive[@]} )) && echo "Archiving ..."
for package_to_archive in "${packages_to_archives[@]}"; do
    echo "${package_to_archive}"
    [[ "${packages[*]}" == *"$(basename "${package_to_archive}")"*  ]] || archive_package "${package_to_archive}"
done

#declare -p

exit 0
